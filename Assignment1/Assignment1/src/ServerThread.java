/**
 *  Name:    Michael Timblin
 *  Project: Assignment1 - Simple socket program to calculate factorial
 *  Purpose: The file contains data and methods to maintain a server socket and
 *           a list of ServerConnectionThread objects.
 */

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

// Maintain a server socket and ServerConnectThread list.
public class ServerThread extends Thread
{
   private int port;
   private MainForm form;
   private ServerSocket listener;
   private List<ServerConnectionThread> threadList;
   private boolean serverFinished = false;

   /**
    *  Constructor: Set the class variables equal to the given values.
    */
   ServerThread(MainForm form, int port)
   {
      this.port = port;
      this.form = form;
   }

   /**
    *  Create a new ServerSocket, and create a new ServerConnectionThread
    *  object for each connection.
    *  Preconditions: none
    */
   @Override
   public void run()
   {
      try
      {
         listener = new ServerSocket(port);
         threadList = new ArrayList<>();
         while (!serverFinished)
         {
            try
            {
               ServerConnectionThread thread = new ServerConnectionThread(
                     listener.accept());
               thread.start();
               threadList.add(thread);
            }
            catch (SocketException e)
            {
               // Server has stopped. There is no need to do anything.
            }
         }
      }
      catch (SocketException e)
      {
         form.displayErrorMessage("Invalid Port!", "Port in use. Please select"
               + " another port.");
         form.disconnect();
      }
      catch (IOException e)
      {
         System.out.println(e);
      }
   }

   /**
    *  Close the ServerSocket object, and close each ServerConnectionThread 
    *  object contained in threadList.
    *  Preconditions: none
    */
   public void disconnect()
   {
      serverFinished = true;
      if (listener != null)
      {
         try
         {
            listener.close();
         }
         catch (IOException e)
         {
            System.out.println(e);
         }
      }
      if (threadList != null)
      {
         for (int i = 0; i < threadList.size(); i++)
         {
            if (threadList.get(i) != null)
            {
               threadList.get(i).disconnect();
            }
         }
      }
   }
}
