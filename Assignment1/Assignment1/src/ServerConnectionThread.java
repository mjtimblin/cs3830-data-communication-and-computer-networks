/**
 *  Name:    Michael Timblin
 *  Project: Assignment1 - Simple socket program to calculate factorial
 *  Purpose: The file contains code for individual server connections. It
 *           returns the factorial of received integers.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.net.SocketException;

// Maintain socket for interracting with a client.
public class ServerConnectionThread extends Thread
{
   private Socket socket;
   private BufferedReader in;
   private PrintWriter out;
   private boolean socketFinished = false;

   /**
    *  Constructor: Set the class variable, "socket," equal to the given value.
    */
   public ServerConnectionThread(Socket socket)
   {
      this.socket = socket;
   }

   /**
    *  Read from the Socket object's input stream, and for each integer, return
    *  the factorial.
    *  Preconditions: none
    */
   @Override
   public void run()
   {
      try
      {
         in = new BufferedReader(new InputStreamReader(
               socket.getInputStream()));
         out = new PrintWriter(socket.getOutputStream(), true);
         while (!socketFinished)
         {
            String input = in.readLine();
            if (input.equals("close"))
            {
               disconnect();
            }
            else
            {
               out.println(factorial(new BigInteger(input)));
            }
         }
      }
      catch (SocketException e)
      {
         // Connect with client closed. There isn't a need to do anything.
      }
      catch (IOException e)
      {
         System.out.println(e);
      }
   }

   /**
    *  Close the Socket object, BufferedReader object, and PrintWriter object.
    *  Preconditions: none
    */
   public void disconnect()
   {
      socketFinished = true;
      if (socket != null && out != null)
      {
         out.println("close");
      }
      try
      {
         socket.close();
         in.close();
         out.close();
      }
      catch (IOException e)
      {
         System.out.println(e);
      }
   }

   /**
    *  Return the factorial of a given BigInteger.
    *  Preconditions: none
    */
   private BigInteger factorial(BigInteger num)
   {
      BigInteger result = new BigInteger("1");
      while (!num.equals(new BigInteger("0")))
      {
         result = result.multiply(num);
         num = num.subtract(new BigInteger("1"));
      }
      return result;
   }
}