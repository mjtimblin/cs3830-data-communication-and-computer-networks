/**
 *  Name:    Michael Timblin
 *  Project: Assignment2 - Socket program with polyalphabetic encryption
 *  Purpose: The file contains contains code to communicate with a server
 *           to obtain a factorial. 
 */

package client;

import java.io.*;
import java.net.*;

// Maintain a socket and update a ClientForm object.
public class ClientThread extends Thread
{
   private BufferedReader in;
   private PrintWriter out;
   private Socket clientSocket;
   private ClientForm clientForm;
   private boolean clientFinished;

   /**
    *  Constructor: Creates a new Socket object with the given IP address
    *               and port.
    */
   ClientThread(ClientForm form, String ip, int port)
   {
      clientForm = form;
      try
      {
         clientSocket = new Socket(ip, port);
         in = new BufferedReader(new InputStreamReader(
               clientSocket.getInputStream()));
         out = new PrintWriter(clientSocket.getOutputStream(), true);
         clientForm.connect();
      }
      catch (ConnectException e)
      {
         clientForm.displayErrorMessage("Connection Error!",
               e.getLocalizedMessage());
         clientForm.disconnect();
      }
      catch (IOException e)
      {
         clientForm.displayErrorMessage("Connection Error!",
               e.getLocalizedMessage());
         clientForm.disconnect();
      }
   }

   /**
    *  Reads from the clientSocket input stream, and calls the clientForm's 
    *  outputText method on each line.
    *  Preconditions: none
    */
   @Override
   public void run()
   {
      while (!clientFinished)
      {
         if (clientSocket != null)
         {
            try
            {
               String message = in.readLine();
               if (message != null)
               {
                  clientForm.outputText(message + "\n");
                  if (message.equals("Good bye!"))
                  {
                     closeSocket();
                  }
               }
            }
            catch (SocketException e)
            {
               // Socket has closed. There is no need to do anything.
            }
            catch (IOException e)
            {
               clientForm.outputText("Error: " + e + "\n");
            }
         }
      }
   }

   /**
    *  Outputs a given stream to the clientSocket output stream.
    *  Preconditions: none
    */
   public void send(String str)
   {
      out.println(str);
   }

   /** 
    *  Sends a "close" message on the Socket's output to ensure the server's
    *  Socket is closed. Calls the closeSocket method.
    *  Preconditions: none
    */
   public void disconnect()
   {
      if (clientSocket != null)
      {
         send("quit");
         closeSocket();
      }
   }

   /**
    *  Close the clientSocket, BufferedReader object ("in"), and PrintWriter 
    *  object ("out").
    *  Preconditions: none
    */
   private void closeSocket()
   {
      clientFinished = true;
      clientForm.disconnect();
      try
      {
         clientSocket.close();
         in.close();
         out.close();
      }
      catch (IOException e)
      {
         clientForm.displayErrorMessage("Disconnection Error!",
               e.getLocalizedMessage());
         clientForm.disconnect();
      }
   }
}