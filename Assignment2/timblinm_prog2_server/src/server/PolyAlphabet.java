/**
 *  Name:    Michael Timblin
 *  Project: Assignment2 - Socket program with polyalphabetic encryption
 *  Purpose: The file contains contains code to encrypt a string with 2 Caesar
 *           cyphers.
 */

package server;

// Encrypt a given string with 2 Caesar cyphers 
public class PolyAlphabet
{
   private final int NUMBER_LETTERS = 26;
   private final int LOWERCASE_A = 97;
   private final int LOWERCASE_Z = 122;
   private final int UPPERCASE_A = 65;
   private final int UPPERCASE_Z = 90;
   private final int[] PATTERN = {0, 1, 1, 0, 1};

   private int cypherZeroOffset;
   private int cypherOneOffset;
   private String plaintext = "";
   
   /**
    *  Constructor: Initializes cypher offsets with given values. 
    */
   public PolyAlphabet(int firstOffset, int secondOffset)
   {
      cypherZeroOffset = firstOffset;
      cypherOneOffset = secondOffset;
   }
   
   /**
    *  This method sets plaintext with the given value.
    *  Preconditions: none
    */
   public void setPlaintext(String plaintext)
   {
      this.plaintext = plaintext;
   }
   
   /**
    *  This method calculates the correct offset to use based on the pattern
    *  and calls getOffsetLetter on each character of plaintext to get the
    *  cyphertext. The method then returns the cyphertext.
    *  Preconditions: none
    */
   public String getCyphertext()
   {
      String cyphertext = "";
      int letterCount = 0;
      for (int i = 0; i < plaintext.length(); i++)
      {
         int offset = 0 == PATTERN[letterCount % PATTERN.length] ?
               cypherZeroOffset : cypherOneOffset;
         cyphertext += getOffsetLetter(plaintext.charAt(i), offset);
         if ((plaintext.charAt(i) >= LOWERCASE_A &&
               plaintext.charAt(i) <= LOWERCASE_Z) ||
               (plaintext.charAt(i) >= UPPERCASE_A &&
               plaintext.charAt(i) <= UPPERCASE_Z))
         {
            letterCount++;
         }
      }
      return cyphertext;
   }
   
   /**
    *  This method adds the given offset to the given character if the character
    *  is a letter. The method then returns either the result if the given
    *  character was a letter or the given character itself.
    *  Preconditions: none
    */
   private char getOffsetLetter(char character, int offset)
   {
      if (character >= LOWERCASE_A && character <= LOWERCASE_Z)
         return (char) ((char) (character - LOWERCASE_A + offset) % 
               NUMBER_LETTERS + LOWERCASE_A);
      else if (character >= UPPERCASE_A && character <= UPPERCASE_Z)
         return (char) ((char) (character - UPPERCASE_A + offset) % 
               NUMBER_LETTERS + UPPERCASE_A);
      else
         return character;
   }
}