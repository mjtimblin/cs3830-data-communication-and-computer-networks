/**
 *  Name:    Michael Timblin
 *  Project: Assignment2 - Socket program with polyalphabetic encryption
 *  Purpose: The file contains code for individual server connections. It calls
 *           a PolyAlphabet object to return an encrypted string for each string
 *           it receives.
 */

package server;

import java.io.*;
import java.net.*;
import java.util.*;

// Maintain socket for interracting with a client
public class ServerThread extends Thread
{
   private Socket clientSock;
   private PrintWriter logFile;
   private PrintWriter out;
   private BufferedReader in;
   private boolean socketFinished = false;
   private PolyAlphabet cypher;
   
   
   public ServerThread (Socket clientSock, PrintWriter logFile)
   {
      this.clientSock = clientSock;
      this.logFile = logFile;
      cypher = new PolyAlphabet(5, 19);
      logFile.println("Got connection: " + new Date() + "  " +
            clientSock.getInetAddress() + "  Port: " + clientSock.getPort());
   }

   /**
    *  Read from the Socket object's input stream, and for each string, return
    *  an encrypted string retrieved from a PolyAlphabet object.
    *  Preconditions: none
    */
   @Override
   public void run()
   {
      try
      {
         in = new BufferedReader(new InputStreamReader(
               clientSock.getInputStream()));
         out = new PrintWriter(clientSock.getOutputStream(), true);
         while (!socketFinished)
         {
            String input = in.readLine();
            if (input != null)
            {
               if (input.equals("quit"))
               {
                  disconnect();
               }
               else
               {
                  cypher.setPlaintext(input);
                  out.println(cypher.getCyphertext());
               }
            } 
         }
      }
      catch (Exception e)
      {
         logFile.println(e);
      }
   }

   /**
    *  Close the Socket object, BufferedReader object, and PrintWriter object.
    *  Preconditions: none
    */
   public void disconnect()
   {
      socketFinished = true;
      if (clientSock != null && out != null)
      {
         out.println("Good bye!");
      }
      try
      {
         logFile.println("Connection closed. Port: " + clientSock.getPort());
         clientSock.close();
         in.close();
         out.close();
      }
      catch (Exception e)
      {
         logFile.println(e);
      }
   }
}