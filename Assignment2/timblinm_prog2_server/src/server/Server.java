/**
 *  Name:    Michael Timblin
 *  Project: Assignment2 - Socket program with polyalphabetic encryption
 *  Purpose: The file contains data and methods to maintain a server socket and
 *           log file.
 */

package server;

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

public class Server
{
   private final int PORT = 5764;
   
   /**
    *  This function is the program's entry point. It creates a Server object
    *  and calls its run method.
    *  Preconditions: none
    */
   public static void main(String args[])
   {
      new Server().run();
   }
   
   /**
    *  This method calls the createLogFile method to retreive a PrintWriter
    *  object for a log file. It then creates a ServerSocket object to listen
    *  for connections. Finally, this method creates a new ServerThread for
    *  each connection to the server.
    *  Preconditions: none
    */
   public void run()
   {
      PrintWriter logFile = createLogFile();
      if (logFile == null)
      {
         JOptionPane.showMessageDialog(null, "Cannot create log file! " + 
               "Run the server again if you wish to continue.",
               "Error!", JOptionPane.INFORMATION_MESSAGE);
         System.exit(0);
      }
      try
      {
         ServerSocket listener = new ServerSocket(PORT);
         while (true)
         {
            new ServerThread(listener.accept(), logFile).start();
         }
      }
      catch (IOException e)
      {
         logFile.println(e + "\n");
      }
   }
   
   /**
    *  This method creates a new file, prog2.log, in the program's directory.
    *  The method then returns a PrintWriter for that file.
    *  Preconditions: none
    */
   private PrintWriter createLogFile()
   {
      try
      {
         String currentDirectory = System.getProperty("user.dir");
         File logFile = new File(currentDirectory + "/prog2.log");
         return new PrintWriter(new FileOutputStream(logFile), true);
      }
      catch (IOException e)
      {
         System.out.println(e);
      }
      return null;
   }
}
